#!/usr/bin/env python
# encoding: utf-8

import re
import os
import click
import git
from path import Path


def isgit(folder):
    return os.path.isdir(folder) and (Path(folder) / ".git").exists()

def get_compare_url(from_branch, to_branch):
    return "TBD"

@click.command()
@click.argument("pattern", default=".*")
@click.argument("folder", nargs=-1)
@click.option("--compare", "-c", help="a branch to compare to")
def gbgrep(pattern, folder, compare):
    """Grep on branches"""
    if folder == ():
        folder = "."
    for d in [Path(d) for d in folder if isgit(d)]:
        repo = git.Repo(d)
        m_branches = set()
        for branch in repo.refs:
            if re.findall(pattern, f"{branch}"):
                m_branches.add(branch)
                msg = f"{d}: {branch}"
                color = "yellow"
                if compare:
                    msg += get_compare_url(branch, compare)
                    color = "red"
                click.secho(msg, fg=color)


if __name__ == "__main__":
    gbgrep()
