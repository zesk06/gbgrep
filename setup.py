#!/usr/bin/env python
# encoding: utf-8

from setuptools import setup, find_packages

setup(
    name='gbgrep',
    version='0.0.1',
    url='https://gitlab.com/zesk06/gbgrep',
    author='Nicolas Rouviere',
    author_email='zesk06@gmail.com',
    description='A git branch grep tool',
    packages=find_packages(),    
    install_requires=["click", "git-python", "path.py"],
    entry_points = {
        "console_scripts": ["gbgrep = gbgrep:gbgrep"],
        }
)
